package com.ruoyi.common.core.service;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.entity.Example;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;

/**
 * @date 2021/11/26 11:18
 * @Description createBy lrh
 */
public abstract class BaseServiceImpl<T> implements BaseService<T> {

    @Autowired
    private Mapper<T> mapper;

    /**
     * 当前泛型真实类型的Class
     */
    private Class<T> modelClass;

    /**
     * 构造方法比其他方法先执行
     */
    public BaseServiceImpl() {
        // 获得具体实体类，通过反射来根据属性条件查找数据
        ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
        modelClass = (Class<T>) pt.getActualTypeArguments()[0];
    }

    @Override
    public int insert(T t) {
        preCreateTime(t);
        return mapper.insertSelective(t);
    }

    @Override
    public int insertOrUpdate(T t) {
        Object id = getId(t);
        if (ObjectUtils.isEmpty(id)) {
            return insert(t);
        }
        return updateById(t);
    }

    @Override
    public int updateById(T t) {
        preUpdateTime(t);
        return mapper.updateByPrimaryKeySelective(t);
    }

    @Override
    public int updateFullById(T t) {
        return mapper.updateByPrimaryKey(t);
    }

    @Override
    public int deleteById(Object o) {
        T t = null;
        try {
            t = modelClass.newInstance();
            preDelFlag(t, 1);
            Field idField = getIdField();
            idField.setAccessible(true);
            // fixme 这里强制id为Long类型，如果不是Long类型会报错，后期考虑优化
            idField.set(t, Long.valueOf(o.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        preUpdateTime(t);
        return mapper.updateByPrimaryKeySelective(t);
    }

    @Override
    public int deleteReallyById(Object o) {
        return mapper.deleteByPrimaryKey(o);
    }


    @Override
    public T getById(Object o) {
        return mapper.selectByPrimaryKey(o);
    }

    @Override
    public List<T> select(T t) {
        return mapper.select(t);
    }

    @Override
    public T getOne(T t) {
        return mapper.selectOne(t);
    }

    @Override
    public T getByField(Object o, String fieldName) {
        Example example = new Example(modelClass);
        Example.Criteria criteria = example.createCriteria();
        if (fieldIsExist()) {
            criteria.andEqualTo("delFlag", 0);
        }
        // fixme 这里强制为String类型，如果不是String类型会报错，后期考虑优化
        Class<?> type = null;
        try {
            type = modelClass.getDeclaredField(fieldName).getType();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        if (type == null || type.equals(String.class)) {
            criteria.andEqualTo(fieldName, o.toString());
        } else if (type.equals(Integer.class)) {
            criteria.andEqualTo(fieldName, Integer.valueOf(o.toString()));
        } else {
            criteria.andEqualTo(fieldName, Long.valueOf(o.toString()));
        }
        return mapper.selectOneByExample(example);
    }

    @Override
    public int deleteByField(Object o, String fieldName) {
        Example example = new Example(modelClass);
        Example.Criteria criteria = example.createCriteria();
        // fixme 这里强制为String类型，如果不是String类型会报错，后期考虑优化
        criteria.andEqualTo(fieldName, o.toString());
        T t = null;
        try {
            t = modelClass.newInstance();
        } catch (Exception e) {
        }
        preDelFlag(t, 1);
        return mapper.updateByExampleSelective(t, example);
    }

    @Override
    public int deleteReallyByField(Object o, String fieldName) {
        Example example = new Example(modelClass);
        Example.Criteria criteria = example.createCriteria();
        // fixme 这里强制为String类型，如果不是String类型会报错，后期考虑优化
        criteria.andEqualTo(fieldName, o.toString());
        return mapper.deleteByExample(example);
    }

    @Override
    public int removeAll() {
        T t = null;
        try {
            t = modelClass.newInstance();
        } catch (Exception e) {
        }
        preDelFlag(t, 1);
        return mapper.updateByExampleSelective(t, new Example(modelClass));
    }

    @Override
    public int removeReallyAll() {
        return mapper.deleteByExample(new Example(modelClass));
    }

    @Override
    public int count(T t) {
        return mapper.selectCount(t);
    }

    /**
     * 添加创建人，创建时间
     */
    private void preCreateTime(T t) {
        preCreateTime(t, DateUtils.getNowDate());
    }

    private void preCreateTime(T t, Date nowDate) {
        try {
            Field createTimeField = modelClass.getSuperclass().getDeclaredField("createTime");
            createTimeField.setAccessible(true);
            Object createTimeObject = createTimeField.get(t);
            if (ObjectUtils.isEmpty(createTimeObject)) {
                createTimeField.set(t, nowDate);
            }
            Field createByField = modelClass.getSuperclass().getDeclaredField("createBy");
            createByField.setAccessible(true);
            Object createByObject = createByField.get(t);
            if (ObjectUtils.isEmpty(createByObject)) {
                createByField.set(t, SecurityUtils.getUsername());
            }
        } catch (Exception e) {
        }
        preDelFlag(t);
    }

    /**
     * 添加更新人，更新时间
     */
    private void preUpdateTime(T t) {
        preUpdateTime(t, DateUtils.getNowDate());
    }

    private void preUpdateTime(T t, Date nowDate) {
        try {
            Field updateTimeFiled = modelClass.getSuperclass().getDeclaredField("updateTime");
            updateTimeFiled.setAccessible(true);
            Object updateTimeObject = updateTimeFiled.get(t);
            if (ObjectUtils.isEmpty(updateTimeObject)) {
                updateTimeFiled.set(t, nowDate);
            }
            Field updateByField = modelClass.getSuperclass().getDeclaredField("updateBy");
            updateByField.setAccessible(true);
            Object updateByObject = updateByField.get(t);
            if (ObjectUtils.isEmpty(updateByObject)) {
                updateByField.set(t, SecurityUtils.getUsername());
            }
        } catch (Exception e) {
        }
    }

    /**
     * 添加删除标志
     */
    private void preDelFlag(T t) {
        preDelFlag(t, 0);
    }

    private void preDelFlag(T t, Integer delFlag) {
        try {
            Field delFlagField = modelClass.getDeclaredField("delFlag");
            delFlagField.setAccessible(true);
            Object delFlagObject = delFlagField.get(t);
            if (ObjectUtils.isEmpty(delFlagObject)) {
                delFlagField.set(t, delFlag);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * 获取id
     */
    private Object getId(T t) {
        Field idField = getIdField();
        if (idField == null) {
            return null;
        }
        idField.setAccessible(true);
        try {
            return idField.get(t);
        } catch (IllegalAccessException e) {
            return null;
        }
    }

    /**
     * 获取标记为Id的属性
     */
    private Field getIdField() {
        Field[] declaredFields = modelClass.getDeclaredFields();
        for (Field field : declaredFields) {
            Id idField = field.getAnnotation(Id.class);
            if (idField != null) {
                return field;
            }
        }
        return null;
    }

    /**
     * 验证删除标志是否存在
     */
    private boolean fieldIsExist() {
        try {
            modelClass.getDeclaredField("delFlag");
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }
}
