package com.ruoyi.common.core.service;

import java.util.List;

/**
 * @date 2021/11/26 11:18
 * @Description createBy lrh
 */
public interface BaseService<T> {

    /**
     * 通用单条插入
     *
     * @return 响应数据
     */
    int  insert(T t);

    /**
     * 通用批量插入
     * @return 响应数据
     */
//    int batchInsert(List<T> list);

    /**
     * 插入或根据id更新
     *
     * @return 响应数据
     */
    int insertOrUpdate(T t);

    /**
     * 通用根据Id更新不为空字段
     *
     * @return 响应数据
     */
    int updateById(T t);

    /**
     * 通用根据Id更新（全部字段）
     *
     * @return 响应数据
     */
    int updateFullById(T t);

    /**
     * 通用根据Id删除 (逻辑删除）
     *
     * @return 响应数据
     */
    int deleteById(Object o);

    /**
     * 通用根据Id删除 (真实删除）
     *
     * @return 响应数据
     */
    int deleteReallyById(Object o);

    /**
     * 通用根据ID查询
     *
     * @return 响应数据
     */
    T getById(Object o);

    /**
     * 通用查询列表
     *
     * @return 响应数据
     */
    List<T> select(T t);

    /**
     * 获取一条信息
     */
    T getOne(T t);

    /**
     * 根据指定属性查询
     */
    T getByField(Object o, String fieldName);

    /**
     * 根据指定属性逻辑删除
     */
    int deleteByField(Object o, String fieldName);

    /**
     * 根据指定属性真实删除
     */
    int deleteReallyByField(Object o, String fieldName);

    /**
     * 删除所有
     */
    int removeAll();

    /**
     * 删除所有
     */
    int removeReallyAll();

    /**
     * 查询总数
     */
    int count(T t);
}
