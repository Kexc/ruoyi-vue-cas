package com.ruoyi.school.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.core.service.BaseServiceImpl;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.school.domain.TreeSelectBookChapterConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.school.mapper.BookChapterConfigMapper;
import com.ruoyi.school.domain.BookChapterConfig;
import com.ruoyi.school.service.IBookChapterConfigService;

/**
 * 章节配置Service业务层处理
 *
 * @author lrh
 * @date 2022-01-06
 */
@Service
public class BookChapterConfigServiceImpl extends BaseServiceImpl<BookChapterConfig> implements IBookChapterConfigService {
    @Autowired
    private BookChapterConfigMapper bookChapterConfigMapper;

    /**
     * 查询章节配置列表
     *
     * @param bookChapterConfig 章节配置
     * @return 章节配置
     */
    @Override
    public List<BookChapterConfig> selectBookChapterConfigList(BookChapterConfig bookChapterConfig) {
        return bookChapterConfigMapper.selectBookChapterConfigList(bookChapterConfig);
    }

    /**
     * 删除章节配置对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBookChapterConfigByIds(Long[] ids) {
        return bookChapterConfigMapper.deleteBookChapterConfigByIds(ids);
    }


    @Override
    public List<TreeSelectBookChapterConfig> buildBookChapterTreeSelect(List<BookChapterConfig> bookChapterConfigList) {
        List<BookChapterConfig> BookChapterConfigTrees = buildBookChapterConfigTree(bookChapterConfigList);
        return BookChapterConfigTrees.stream().map(TreeSelectBookChapterConfig::new).collect(Collectors.toList());
    }

    /**
     * 构建前端所需要树结构
     *
     * @return 树结构列表
     */
    public List<BookChapterConfig> buildBookChapterConfigTree(List<BookChapterConfig> bookChapterConfigList) {
        List<BookChapterConfig> returnList = new ArrayList<BookChapterConfig>();
        List<Long> tempList = new ArrayList<Long>();
        for (BookChapterConfig BookChapterConfig : bookChapterConfigList) {
            tempList.add(BookChapterConfig.getBookChapterConfigId());
        }
        for (Iterator<BookChapterConfig> iterator = bookChapterConfigList.iterator(); iterator.hasNext(); ) {
            BookChapterConfig BookChapterConfig = (BookChapterConfig) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(BookChapterConfig.getParentId())) {
                recursionFn(bookChapterConfigList, BookChapterConfig);
                returnList.add(BookChapterConfig);
            }
        }
        if (returnList.isEmpty()) {
            returnList = bookChapterConfigList;
        }
        return returnList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<BookChapterConfig> list, BookChapterConfig bookChapterConfig) {
        // 得到子节点列表
        List<BookChapterConfig> childList = getChildList(list, bookChapterConfig);
        bookChapterConfig.setChildren(childList);
        for (BookChapterConfig tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<BookChapterConfig> getChildList(List<BookChapterConfig> list, BookChapterConfig bookChapterConfig) {
        List<BookChapterConfig> tlist = new ArrayList<BookChapterConfig>();
        Iterator<BookChapterConfig> it = list.iterator();
        while (it.hasNext()) {
            BookChapterConfig n = (BookChapterConfig) it.next();
            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == bookChapterConfig.getBookChapterConfigId()) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<BookChapterConfig> list, BookChapterConfig bookChapterConfig) {
        return getChildList(list, bookChapterConfig).size() > 0 ? true : false;
    }

    @Override
    public List<Long> selectAllParentsBookChapterConfigList(Long id) {
        return bookChapterConfigMapper.selectAllParentsBookChapterConfigList(id);
    }

    @Override
    public List<Long> selectAllChildrenBookChapterConfigList(Long id) {
        return bookChapterConfigMapper.selectAllChildrenBookChapterConfigList(id);
    }
}
