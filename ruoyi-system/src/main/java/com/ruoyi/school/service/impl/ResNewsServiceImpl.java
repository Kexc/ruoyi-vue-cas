package com.ruoyi.school.service.impl;

import java.util.List;
import com.ruoyi.common.core.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.school.mapper.ResNewsMapper;
import com.ruoyi.school.domain.ResNews;
import com.ruoyi.school.service.IResNewsService;

/**
 * 资源库新闻Service业务层处理
 *
 * @author lrh
 * @date 2022-01-24
 */
@Service
public class ResNewsServiceImpl extends BaseServiceImpl<ResNews> implements IResNewsService
{
    @Autowired
    private ResNewsMapper resNewsMapper;

    /**
     * 查询资源库新闻列表
     *
     * @param resNews 资源库新闻
     * @return 资源库新闻
     */
    @Override
    public List<ResNews> selectResNewsList(ResNews resNews)
    {
        return resNewsMapper.selectResNewsList(resNews);
    }
    /**
     * 批量删除资源库新闻
     *
     * @param newsIds 需要删除的资源库新闻主键
     * @return 结果
     */
            @Override
    public int deleteResNewsByIds(Long[] newsIds)
    {
                return resNewsMapper.deleteResNewsByIds(newsIds);
    }
    }
