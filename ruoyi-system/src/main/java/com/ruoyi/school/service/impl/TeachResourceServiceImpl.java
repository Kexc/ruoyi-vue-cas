package com.ruoyi.school.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.core.service.BaseServiceImpl;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.school.domain.BookChapterConfig;
import com.ruoyi.school.mapper.BookChapterConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.school.mapper.TeachResourceMapper;
import com.ruoyi.school.domain.TeachResource;
import com.ruoyi.school.service.ITeachResourceService;

/**
 * 资源库Service业务层处理
 *
 * @author lrh
 * @date 2022-01-07
 */
@Service
public class TeachResourceServiceImpl extends BaseServiceImpl<TeachResource> implements ITeachResourceService {
    @Autowired
    private TeachResourceMapper teachResourceMapper;
    @Autowired
    private BookChapterConfigMapper bookChapterConfigMapper;

    /**
     * 查询资源库列表
     *
     * @param teachResource 资源库
     * @return 资源库
     */
    @Override
    public List<TeachResource> selectTeachResourceList(TeachResource teachResource, List<Long> bookChapterConfigIds) {
        return teachResourceMapper.selectTeachResourceList(teachResource, bookChapterConfigIds);
    }

    @Override
    public TeachResource selectTeachResourceById(Long teachResourceId) {
        return teachResourceMapper.selectTeachResourceById(teachResourceId);
    }

    @Override
    public int addViewNum(Long teachResourceId) {
        return teachResourceMapper.addViewNum(teachResourceId);
    }

    /**
     * 批量删除资源库
     *
     * @param teachResourceIds 需要删除的资源库主键
     * @return 结果
     */
    @Override
    public int deleteTeachResourceByIds(Long[] teachResourceIds) {
        return teachResourceMapper.deleteTeachResourceByIds(teachResourceIds);
    }
}
