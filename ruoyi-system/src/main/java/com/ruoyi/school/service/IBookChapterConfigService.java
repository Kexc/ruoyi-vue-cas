package com.ruoyi.school.service;

import java.util.List;

import com.ruoyi.common.core.service.BaseService;
import com.ruoyi.school.domain.BookChapterConfig;
import com.ruoyi.school.domain.TreeSelectBookChapterConfig;

/**
 * 章节配置Service接口
 *
 * @author lrh
 * @date 2022-01-06
 */
public interface IBookChapterConfigService extends BaseService<BookChapterConfig> {

    /**
     * 查询章节配置列表
     *
     * @param bookChapterConfig 章节配置
     * @return 章节配置集合
     */
    public List<BookChapterConfig> selectBookChapterConfigList(BookChapterConfig bookChapterConfig);

    /**
     * 批量删除章节配置
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBookChapterConfigByIds(Long[] ids);

    /**
     * 构建前端所需要下拉树结构
     *
     * @return 下拉树结构列表
     */
    public List<TreeSelectBookChapterConfig> buildBookChapterTreeSelect(List<BookChapterConfig> bookChapterConfigList);


    List<Long> selectAllParentsBookChapterConfigList(Long id);

    List<Long> selectAllChildrenBookChapterConfigList(Long id);

}
