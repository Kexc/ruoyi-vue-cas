package com.ruoyi.school.service;

import java.util.List;

import com.ruoyi.school.domain.TeachResource;
import com.ruoyi.common.core.service.BaseService;

/**
 * 资源库Service接口
 *
 * @author lrh
 * @date 2022-01-07
 */
public interface ITeachResourceService extends BaseService<TeachResource> {

    /**
     * 查询资源库列表
     *
     * @param teachResource 资源库
     * @return 资源库集合
     */
    public List<TeachResource> selectTeachResourceList(TeachResource teachResource,List<Long> bookChapterConfigIds);
    TeachResource selectTeachResourceById(Long teachResourceId);
   int addViewNum(Long teachResourceId);
    /**
     * 批量删除资源库
     *
     * @param teachResourceIds 需要删除的资源库主键集合
     * @return 结果
     */
    public int deleteTeachResourceByIds(Long[] teachResourceIds);
}
