package com.ruoyi.school.domain;

import lombok.*;

import javax.persistence.*;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资源库新闻对象 res_news
 *
 * @author lrh
 * @date 2022-01-24
 */
@Data
@Table(name = "res_news")
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ResNews extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 新闻ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long newsId;

    /**
     * 标题
     */
    @Excel(name = "标题")
    @Column(name = "title")
    private String title;

    /**
     * 缩略图
     */
    @Excel(name = "缩略图")
    @Column(name = "thumb_url")
    private String thumbUrl;

    /**
     * 内容
     */
    @Excel(name = "内容")
    @Column(name = "content")
    private String content;

    /**
     * 是否置顶
     */
    @Excel(name = "是否置顶")
    @Column(name = "is_top")
    private String isTop;
}
