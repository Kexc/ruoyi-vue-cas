package com.ruoyi.school.domain;

import lombok.*;

import javax.persistence.*;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资源库对象 teach_resource
 *
 * @author lrh
 * @date 2022-01-07
 */
@Data
@Table(name = "teach_resource")
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TeachResource extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long teachResourceId;

    /**
     * 资源类型
     */
    @Excel(name = "资源类型")
    @Column(name = "res_type")
    private String resType;

    /**
     * 章节ID
     */
    @Excel(name = "章节ID")
    @Column(name = "book_chapter_id")
    private Long bookChapterId;

    @Transient
    private String chapterTitle;
    /**
     * 年级ID
     */
    @Excel(name = "年级ID")
    @Column(name = "grade_id")
    private Long gradeId;

    /**
     * 名称
     */
    @Excel(name = "名称")
    @Column(name = "title")
    private String title;
    /**
     * 作者
     */
    @Excel(name = "名称")
    @Column(name = "res_author")
    private String resAuthor;
    /**
     * 缩略图地址
     */
    @Excel(name = "缩略图地址")
    @Column(name = "thumb_url")
    private String thumbUrl;

    /**
     * 学科
     */
    @Excel(name = "学科")
    @Column(name = "course_type")
    private String courseType;

    /**
     * 上传用户ID
     */
    @Excel(name = "上传用户ID")
    @Column(name = "user_id")
    private Long userId;
    @Transient
    private String userName;
    /**
     * 文件查看类型
     */
    @Excel(name = "文件查看类型")
    @Column(name = "file_type")
    private String fileType;

    /**
     * 文件路径
     */
    @Excel(name = "文件路径")
    @Column(name = "file_path")
    private String filePath;

    /**
     * 文件类型
     */
    @Excel(name = "文件类型")
    @Column(name = "file_ext")
    private String fileExt;

    /**
     * 文件大小
     */
    @Excel(name = "文件大小")
    @Column(name = "file_size")
    private String fileSize;

    /**
     * 查看次数
     */
    @Excel(name = "查看次数")
    @Column(name = "view_num")
    private Long viewNum;

    /**
     * 下载次数
     */
    @Excel(name = "下载次数")
    @Column(name = "down_num")
    private Long downNum;

    /**
     * 评分
     */
    @Excel(name = "评分")
    @Column(name = "plan_score")
    private Long planScore;
}
