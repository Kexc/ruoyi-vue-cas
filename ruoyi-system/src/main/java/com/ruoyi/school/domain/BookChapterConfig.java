package com.ruoyi.school.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.domain.TreeEntity;
import lombok.*;

import javax.persistence.*;

import javax.persistence.Column;
import java.util.ArrayList;
import java.util.List;

/**
 * 章节配置对象 book_chapter_config
 *
 * @author lrh
 * @date 2022-01-06
 */
@Data
@Table(name = "book_chapter_config")
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BookChapterConfig extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookChapterConfigId;

    /**
     * 章节名称
     */
    @Excel(name = "章节名称")
    @Column(name = "chapter_title")
    private String chapterTitle;

    /**
     * 年级ID
     */
    @Excel(name = "年级ID")
    @Column(name = "grade_id")
    private Long gradeId;
    /**
     * 上级ID
     */
    @Excel(name = "上级ID")
    @Column(name = "parent_id")
    private Long parentId;
    /**
     * 所属学科
     */
    @Excel(name = "所属学科")
    @Column(name = "course_type")
    private String courseType;
    /**
     * 排序
     */
    @Excel(name = "排序")
    @Column(name = "order_num")
    private Integer orderNum;
    /**
     * 子菜单
     */
    @Transient
    private List<BookChapterConfig> children = new ArrayList<BookChapterConfig>();
}
