package com.ruoyi.school.mapper;

import java.util.List;

import tk.mybatis.mapper.common.Mapper;
import com.ruoyi.school.domain.BookChapterConfig;

/**
 * 章节配置Mapper接口
 *
 * @author lrh
 * @date 2022-01-06
 */
public interface BookChapterConfigMapper extends Mapper<BookChapterConfig> {

    /**
     * 查询章节配置列表
     *
     * @param bookChapterConfig 章节配置
     * @return 章节配置集合
     */
    public List<BookChapterConfig> selectBookChapterConfigList(BookChapterConfig bookChapterConfig);

    /**
     * 批量删除章节配置
     *
     * @param bookChapterConfigIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBookChapterConfigByIds(Long[] bookChapterConfigIds);

    List<Long> selectAllChildrenBookChapterConfigList(Long id);

    List<Long> selectAllParentsBookChapterConfigList(Long id);
}
