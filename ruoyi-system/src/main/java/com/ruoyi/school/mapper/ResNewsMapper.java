package com.ruoyi.school.mapper;

import java.util.List;

import tk.mybatis.mapper.common.Mapper;
import com.ruoyi.school.domain.ResNews;

/**
 * 资源库新闻Mapper接口
 *
 * @author lrh
 * @date 2022-01-24
 */
public interface ResNewsMapper extends Mapper<ResNews> {

    /**
     * 查询资源库新闻列表
     *
     * @param resNews 资源库新闻
     * @return 资源库新闻集合
     */
    public List<ResNews> selectResNewsList(ResNews resNews);

    /**
     * 批量删除资源库新闻
     *
     * @param newsIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResNewsByIds(Long[] newsIds);
    }
