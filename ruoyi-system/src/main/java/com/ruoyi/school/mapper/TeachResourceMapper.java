package com.ruoyi.school.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;
import com.ruoyi.school.domain.TeachResource;

/**
 * 资源库Mapper接口
 *
 * @author lrh
 * @date 2022-01-07
 */
public interface TeachResourceMapper extends Mapper<TeachResource> {

    /**
     * 查询资源库列表
     *
     * @param teachResource 资源库
     * @return 资源库集合
     */
    public List<TeachResource> selectTeachResourceList(@Param("teachResource") TeachResource teachResource, @Param("bookChapterConfigIds") List<Long> bookChapterConfigIds);

    TeachResource selectTeachResourceById(Long teachResourceId);

    int addViewNum(Long teachResourceId);

    /**
     * 批量删除资源库
     *
     * @param teachResourceIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTeachResourceByIds(Long[] teachResourceIds);
}
