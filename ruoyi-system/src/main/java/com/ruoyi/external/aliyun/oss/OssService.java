package com.ruoyi.external.aliyun.oss;

import com.aliyun.oss.model.OSSObjectSummary;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

/**
 * OSS服务
 *
 * @author lrh
 * @date 2021-04-21
 */
public interface OssService {

    String getLoadUrl();

    /**
     * 列举文件
     *
     * @param prefix 匹配字符，一般传文件夹名
     * @return 文件集合
     */
    List<OSSObjectSummary> queryAll(String prefix);

    /**
     * 获取某文件、文件夹的大小
     *
     * @param prefix 匹配字符，一般传文件夹名
     * @return (MB)
     */
    BigDecimal getSize(String prefix);

    /**
     * 删除文件
     *
     * @param url OSS访问路径
     */
    void deleteFile(String url);

    /**
     * 删除文件夹
     *
     * @param folder 文件夹
     */
    void deleteFolder(String folder);


    /**
     * MultipartFile上传
     *
     * @param multipartFile 上传的文件对象
     * @param fileName      要保存的文件名
     * @return 网络访问路径
     * @throws Exception 调用失败
     */
    String uploadMultipartFile(String fileName, MultipartFile multipartFile) throws Exception;

    /**
     * MultipartFile上传
     *
     * @param multipartFile 上传的文件对象
     * @param fileName      要保存的文件名
     * @return 网络访问路径
     * @throws Exception 调用失败
     */
    String uploadMultipartFile(String fileName, MultipartFile multipartFile, CharSequence... folder) throws Exception;

    /**
     * MultipartFile上传
     *
     * @param multipartFile 上传的文件对象
     * @param fileName      要保存的文件名 ps：需要命名有意义，这样才能去覆盖源文件，如用户的头像，可以将名字设置为 'avatar'
     * @param folder        文件夹名 ps：如果上传用户信息，需要将文件夹名指定为用户编号，方便用户注销后删除
     * @return 网络访问路径
     * @throws Exception 调用失败
     */
    String uploadMultipartFile(String folder, String fileName, MultipartFile multipartFile) throws Exception;

    /**
     * 本地路径上传
     *
     * @param localFilePath 文件的本地路径
     * @param fileName      要保存的文件名
     * @return 网络访问路径
     * @throws FileNotFoundException 本地文件未找到
     */
    String uploadSimple(String fileName, String localFilePath) throws FileNotFoundException;

    /**
     * 本地路径上传
     *
     * @param localFilePath 文件的本地路径
     * @param fileName      要保存的文件名 ps：需要命名有意义，这样才能去覆盖源文件，如用户的头像，可以将名字设置为 'avatar'
     * @param folder        文件夹名 ps：如果上传用户信息，需要将文件夹名指定为用户编号，方便用户注销后删除
     * @return 网络访问路径
     * @throws FileNotFoundException 本地文件未找到
     */
    String uploadSimple(String folder, String fileName, String localFilePath) throws FileNotFoundException;

    /**
     * 文件流上传
     *
     * @param inputStream 文件流
     * @param fileName    要保存的文件名
     * @return 网络访问路径
     */
    String uploadSimple(String fileName, InputStream inputStream);

    /**
     * 文件流上传
     *
     * @param inputStream 文件流
     * @param fileName    要保存的文件名(需要指定文件类型)
     * @param folder      文件夹名
     * @return 网络访问路径
     */
    String uploadSimple(String folder, String fileName, InputStream inputStream);
}
