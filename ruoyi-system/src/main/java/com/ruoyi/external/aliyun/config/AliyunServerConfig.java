package com.ruoyi.external.aliyun.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 阿里云服务 配置类
 *
 * @author lrh
 * @date 2021-04-19
 */
@Configuration
public class AliyunServerConfig {

    @Value("${aliyun.accessKeyId}")
    private String accessKeyId;

    @Value("${aliyun.accessKeySecret}")
    private String accessKeySecret;

    /**
     * OSS服务地域
     */
    @Value("${aliyun.oss.endpoint}")
    private String ossEndpoint;

    /**
     * 注册OSS服务Bean
     */
    @Bean
    public OSS initOssServer() {
        return new OSSClientBuilder().build(ossEndpoint, accessKeyId, accessKeySecret);
    }

}
