package com.ruoyi.external.aliyun.oss.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.ListObjectsRequest;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.ruoyi.external.aliyun.oss.OssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * OSS服务
 *
 * @author lrh
 * @date 2021-04-21
 */
@Service
public class OssServiceImpl extends OssCommonServiceImpl implements OssService {

    @Autowired
    private OSS client;

    @Override
    public String getLoadUrl() {
        return this.loadUrl;
    }

    @Override
    public List<OSSObjectSummary> queryAll(String prefix) {
        List<OSSObjectSummary> ossObjectSummaries = query(prefix);
        if (CollectionUtils.isEmpty(ossObjectSummaries)) {
            return new ArrayList<>();
        }
        for (OSSObjectSummary ob : ossObjectSummaries) {
            ob.setKey(loadUrl + ob.getKey());
        }
        return ossObjectSummaries;
    }

    @Override
    public BigDecimal getSize(String prefix) {
        List<OSSObjectSummary> ossObjectSummaries = queryAll(prefix);
        BigDecimal allSize = BigDecimal.ZERO;
        if (CollectionUtils.isEmpty(ossObjectSummaries)) {
            return allSize;
        }

        for (OSSObjectSummary ob : ossObjectSummaries) {
            allSize = allSize.add(BigDecimal.valueOf(ob.getSize()));
        }
        // Byte 转化为 MB
        return allSize.divide(BigDecimal.valueOf(1024 * 1024), 4, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public void deleteFile(String url) {
        client.deleteObject(bucketName, url.replace(loadUrl, ""));
    }

    @Override
    public void deleteFolder(String folder) {
        List<OSSObjectSummary> ossObjectSummaries = queryAll(folder);
        if (CollectionUtils.isEmpty(ossObjectSummaries)) {
            return;
        }
        for (OSSObjectSummary oos : ossObjectSummaries) {
            deleteFile(oos.getKey());
        }
        client.deleteObject(bucketName, folder);
    }

    @Override
    public String uploadMultipartFile(String fileName, MultipartFile file) throws Exception {
        return uploadMultipartFile("", fileName, file);
    }

    @Override
    public String uploadMultipartFile(String fileName, MultipartFile multipartFile, CharSequence... folder) throws Exception {
        return uploadMultipartFile(String.join("/", folder), fileName, multipartFile);
    }

    @Override
    public String uploadMultipartFile(String folder, String fileName, MultipartFile file) throws Exception {
        if (!existSuffix(fileName)) {
            fileName += getSuffix(file.getOriginalFilename());
        }
        return uploadSimple(folder, fileName, file.getInputStream());
    }

    @Override
    public String uploadSimple(String fileName, String localFilePath) throws FileNotFoundException {
        return uploadSimple("", fileName, localFilePath);
    }

    @Override
    public String uploadSimple(String folder, String fileName, String localFilePath) throws FileNotFoundException {
        if (!existSuffix(fileName)) {
            fileName += getSuffix(localFilePath);
        }
        InputStream inputStream = new FileInputStream(localFilePath);
        return uploadSimple(folder, fileName, inputStream);
    }

    @Override
    public String uploadSimple(String fileName, InputStream inputStream) {
        return uploadSimple("", fileName, inputStream);
    }

    @Override
    public String uploadSimple(String folder, String fileName, InputStream inputStream) {
        // 格式化文件夹
        folder = formatFolder(folder);
        // 格式化文件名
        client.putObject(bucketName, folder + fileName, inputStream);
        return loadUrl + folder + fileName;
    }

    /**
     * 查询OSS文件夹下总数
     */
    List<OSSObjectSummary> query(String prefix) {
        if (StringUtils.isEmpty(prefix)) {
            return null;
        }
        String nextMarker = null;
        ObjectListing objectListing;
        List<OSSObjectSummary> sumAll = new ArrayList<>();
        do {
            objectListing = client.listObjects(new ListObjectsRequest(bucketName).withMarker(nextMarker).withMaxKeys(200).withPrefix(prefix));
            List<OSSObjectSummary> sums = objectListing.getObjectSummaries();
            if (!CollectionUtils.isEmpty(sums)) {
                sumAll.addAll(sums);
            }
            nextMarker = objectListing.getNextMarker();
        } while (objectListing.isTruncated());

        return sumAll;
    }
}
