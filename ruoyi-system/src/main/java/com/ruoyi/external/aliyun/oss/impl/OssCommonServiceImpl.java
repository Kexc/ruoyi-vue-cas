package com.ruoyi.external.aliyun.oss.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class OssCommonServiceImpl {

    /**
     * 默认文件夹名称
     */
    public String defaultFolder = "default";

    /**
     * 分隔符
     */
    public String delimiter = "/";

    /**
     * OSS服务地域
     */
    @Value("${aliyun.oss.endpoint}")
    private String ossEndpoint;

    /**
     * bucketName
     */
    @Value("${aliyun.oss.bucketName}")
    public String bucketName;

    /**
     * OSS访问地址前缀
     */
    public String loadUrl;

    /**
     * 初始化loadUrl
     */
    @PostConstruct
    public void init() {
        // loadUrl 是 bucketName endpoint 组合而成
        this.loadUrl = ossEndpoint.replace("://", "://" + bucketName + ".");
    }

    /**
     * 格式化文件夹
     *
     * @param folder 文件夹名称
     */
    public String formatFolder(String folder) {
        if (StringUtils.isBlank(folder)) {
            return formatFolder();
        }
        int length = folder.length();
        if (delimiter.equals(folder.substring(length - 1))) {
            return folder;
        }
        return new StringBuffer(folder).append(delimiter).toString();
    }

    /**
     * 获取默认文件夹
     */
    public String formatFolder() {
        return defaultFolder;
    }

    /**
     * 文件名是否有后缀
     */
    public boolean existSuffix(String fileName) {
        int splitIndex = fileName.lastIndexOf(".");
        if (splitIndex == -1) {
            return false;
        }
        return true;
    }

    /**
     * 获取后缀
     */
    public String getSuffix(String fileName) {
        int splitIndex = fileName.lastIndexOf(".");
        if (splitIndex == -1) {
            return "";
        }
        return fileName.substring(splitIndex);
    }
}
