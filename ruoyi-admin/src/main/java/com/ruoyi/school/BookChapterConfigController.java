package com.ruoyi.school;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.school.domain.BookChapterConfig;
import com.ruoyi.school.service.IBookChapterConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 章节配置Controller
 *
 * @author lrh
 * @date 2022-01-06
 */
@RestController
@RequestMapping("/school/bookChapterConfig")
public class BookChapterConfigController extends BaseController {
    @Autowired
    private IBookChapterConfigService bookChapterConfigService;

    /**
     * 查询章节配置列表
     */
    @PreAuthorize("@ss.hasPermi('school:bookChapterConfig:list')")
    @GetMapping("/list")
    public AjaxResult list(BookChapterConfig bookChapterConfig) {
        List<BookChapterConfig> list = bookChapterConfigService.selectBookChapterConfigList(bookChapterConfig);
        return AjaxResult.success(list);
    }

    /**
     * 查询章节配置列表
     */
    @GetMapping("/treeselect")
    public AjaxResult treeselect(BookChapterConfig bookChapterConfig) {
        List<BookChapterConfig> list = bookChapterConfigService.selectBookChapterConfigList(bookChapterConfig);
        return AjaxResult.success(bookChapterConfigService.buildBookChapterTreeSelect(list));
    }

    /**
     * 导出章节配置列表
     */
    @PreAuthorize("@ss.hasPermi('school:bookChapterConfig:export')")
    @Log(title = "章节配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BookChapterConfig bookChapterConfig) {
        List<BookChapterConfig> list = bookChapterConfigService.selectBookChapterConfigList(bookChapterConfig);
        ExcelUtil<BookChapterConfig> util = new ExcelUtil<BookChapterConfig>(BookChapterConfig.class);
        return util.exportExcel(list, "章节配置数据");
    }

    /**
     * 获取章节配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('school:bookChapterConfig:query')")
    @GetMapping(value = "/{bookChapterConfigId}")
    public AjaxResult getInfo(@PathVariable("bookChapterConfigId") Long bookChapterConfigId) {
        return AjaxResult.success(bookChapterConfigService.getById(bookChapterConfigId));
    }

    /**
     * 新增章节配置
     */
    @PreAuthorize("@ss.hasPermi('school:bookChapterConfig:add')")
    @Log(title = "章节配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BookChapterConfig bookChapterConfig) {
        bookChapterConfig.setCreateBy(getUsername());
        bookChapterConfig.setCreateTime(DateUtils.getNowDate());
        List<Long> longs = bookChapterConfigService.selectAllParentsBookChapterConfigList(bookChapterConfig.getParentId());
        if (longs.size() >= 3) {
            return AjaxResult.error("不可超过三级");
        }
        return toAjax(bookChapterConfigService.insert(bookChapterConfig));
    }

    /**
     * 修改章节配置
     */
    @PreAuthorize("@ss.hasPermi('school:bookChapterConfig:edit')")
    @Log(title = "章节配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BookChapterConfig bookChapterConfig) {
        List<Long> longs = bookChapterConfigService.selectAllParentsBookChapterConfigList(bookChapterConfig.getParentId());
        if (longs.size() >= 3) {
            return AjaxResult.error("不可超过三级");
        }
        return toAjax(bookChapterConfigService.updateById(bookChapterConfig));
    }

    /**
     * 删除章节配置
     */
    @PreAuthorize("@ss.hasPermi('school:bookChapterConfig:remove')")
    @Log(title = "章节配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{bookChapterConfigIds}")
    public AjaxResult remove(@PathVariable Long[] bookChapterConfigIds) {
        return toAjax(bookChapterConfigService.deleteBookChapterConfigByIds(bookChapterConfigIds));
    }
}
