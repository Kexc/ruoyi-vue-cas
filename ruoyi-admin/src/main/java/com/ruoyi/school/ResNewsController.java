package com.ruoyi.school;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.school.domain.ResNews;
import com.ruoyi.school.service.IResNewsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资源库新闻Controller
 *
 * @author lrh
 * @date 2022-01-24
 */
@RestController
@RequestMapping("/school/news")
public class ResNewsController extends BaseController {
    @Autowired
    private IResNewsService resNewsService;

    /**
     * 查询资源库新闻列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ResNews resNews) {
        startPage();
        List<ResNews> list = resNewsService.selectResNewsList(resNews);
        return getDataTable(list);
    }

    /**
     * 导出资源库新闻列表
     */
    @PreAuthorize("@ss.hasPermi('school:news:export')")
    @Log(title = "资源库新闻", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ResNews resNews) {
        List<ResNews> list = resNewsService.selectResNewsList(resNews);
        ExcelUtil<ResNews> util = new ExcelUtil<ResNews>(ResNews.class);
        return util.exportExcel(list, "资源库新闻数据");
    }

    /**
     * 获取资源库新闻详细信息
     */
    @GetMapping(value = "/{newsId}")
    public AjaxResult getInfo(@PathVariable("newsId") Long newsId) {
        return AjaxResult.success(resNewsService.getById(newsId));
    }

    /**
     * 新增资源库新闻
     */
    @PreAuthorize("@ss.hasPermi('school:news:add')")
    @Log(title = "资源库新闻", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ResNews resNews) {
        return toAjax(resNewsService.insert(resNews));
    }

    /**
     * 修改资源库新闻
     */
    @PreAuthorize("@ss.hasPermi('school:news:edit')")
    @Log(title = "资源库新闻", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ResNews resNews) {
        return toAjax(resNewsService.updateById(resNews));
    }

    /**
     * 删除资源库新闻
     */
    @PreAuthorize("@ss.hasPermi('school:news:remove')")
    @Log(title = "资源库新闻", businessType = BusinessType.DELETE)
    @DeleteMapping("/{newsIds}")
    public AjaxResult remove(@PathVariable Long[] newsIds) {
        return toAjax(resNewsService.deleteResNewsByIds(newsIds));
    }
}
