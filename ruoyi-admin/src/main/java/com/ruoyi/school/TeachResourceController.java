package com.ruoyi.school;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.school.service.IBookChapterConfigService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.school.domain.TeachResource;
import com.ruoyi.school.service.ITeachResourceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资源库Controller
 *
 * @author lrh
 * @date 2022-01-07
 */
@RestController
@RequestMapping("/school/resource")
public class TeachResourceController extends BaseController {
    @Autowired
    private ITeachResourceService teachResourceService;
    @Autowired
    private IBookChapterConfigService bookChapterConfigService;

    /**
     * 查询资源库列表
     */
    @GetMapping("/list")
    public TableDataInfo list(TeachResource teachResource) {
        List<Long> bookChapterConfigIds = new ArrayList<>();
        if (teachResource.getBookChapterId() != null && StringUtils.isNotEmpty(teachResource.getBookChapterId().toString())) {
            bookChapterConfigIds.add(teachResource.getBookChapterId());
            List<Long> longs = bookChapterConfigService.selectAllChildrenBookChapterConfigList(teachResource.getBookChapterId());
            bookChapterConfigIds.addAll(longs);
        }
        startPage();
        List<TeachResource> list = teachResourceService.selectTeachResourceList(teachResource, bookChapterConfigIds);
        return getDataTable(list);
    }

    /**
     * 导出资源库列表
     */
    @PreAuthorize("@ss.hasPermi('school:resource:export')")
    @Log(title = "资源库", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TeachResource teachResource) {
        List<TeachResource> list = teachResourceService.selectTeachResourceList(teachResource, null);
        ExcelUtil<TeachResource> util = new ExcelUtil<TeachResource>(TeachResource.class);
        return util.exportExcel(list, "资源库数据");
    }

    /**
     * 获取资源库详细信息
     */
    @PreAuthorize("@ss.hasPermi('school:resource:query')")
    @GetMapping(value = "/{teachResourceId}")
    public AjaxResult getInfo(@PathVariable("teachResourceId") Long teachResourceId) {
        return AjaxResult.success(teachResourceService.selectTeachResourceById(teachResourceId));
    }

    /**
     * 获取资源库详细信息
     */
    @GetMapping(value = "/getInfoWeb/{teachResourceId}")
    public AjaxResult getInfoWeb(@PathVariable("teachResourceId") Long teachResourceId) {
        teachResourceService.addViewNum(teachResourceId);
        return AjaxResult.success(teachResourceService.selectTeachResourceById(teachResourceId));
    }

    /**
     * 新增资源库
     */
    @PreAuthorize("@ss.hasPermi('school:resource:add')")
    @Log(title = "资源库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TeachResource teachResource) {
        teachResource.setUserId(getUserId());
        return toAjax(teachResourceService.insert(teachResource));
    }

    /**
     * 修改资源库
     */
    @PreAuthorize("@ss.hasPermi('school:resource:edit')")
    @Log(title = "资源库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TeachResource teachResource) {
        return toAjax(teachResourceService.updateById(teachResource));
    }

    /**
     * 删除资源库
     */
    @PreAuthorize("@ss.hasPermi('school:resource:remove')")
    @Log(title = "资源库", businessType = BusinessType.DELETE)
    @DeleteMapping("/{teachResourceIds}")
    public AjaxResult remove(@PathVariable Long[] teachResourceIds) {
        return toAjax(teachResourceService.deleteTeachResourceByIds(teachResourceIds));
    }
}
